﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;

namespace Sykj.Test.Api.v1
{
    /// <summary>
    /// 测试留言信息
    /// </summary>
    public class GuestBook : IDisposable
    {
        IServiceProvider _serviceProvider;
        IGuestbook _guestbook;
        IUsers _users;
        IFavorite _favorite;
        GuestBookController _guestBookController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public GuestBook()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _guestbook = _serviceProvider.GetService<IGuestbook>();
            _users = _serviceProvider.GetService<IUsers>();
            _favorite = _serviceProvider.GetService<IFavorite>();
            _guestBookController = new GuestBookController(_guestbook, _users, _favorite);//实例化
        }

        /// <summary>
        /// 测试搜索功能  bjg
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="type">new 最新 hot最热</param>
        /// <param name="keyWords">搜索条件</param>
        [Theory]
        [InlineData(1, 5, 1, "")]
        [InlineData(1, 5, 2, "测试")]
        public void NewGuestbookList(int pageIndex, int pageSize, int type, string keyWords)
        {
            ApiResult result = _guestBookController.GuestbookList(pageIndex, pageSize, type, keyWords);
            Assert.True(result.success);
        }

        /// <summary>
        /// 添加问题
        /// </summary>
        [Fact]
        public void Add()
        {
            _guestBookController.UserId = 82;
            ApiResult result = _guestBookController.AddGuestBook("测试问题3");
            Assert.True(result.success);
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        [Fact]
        public void GuestBookDetail()
        {
            ApiResult result = _guestBookController.GuestBookDetail(2);
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除数据
        /// </summary>
        public void Dispose()
        {
            //清除数据
        }
    }
}
