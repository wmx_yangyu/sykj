﻿using Sykj.Infrastructure;
using Sykj.IServices;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;
using static Sykj.ViewModel.EnumHelper;

namespace Sykj.Test.Api.v1
{
    /// <summary>
    /// 订单管理
    /// </summary>
    public class Message : IDisposable
    {
        IServiceProvider _serviceProvider;
        ISiteMessage _siteMessage;
        IUsers _users;
        IUserMember _userMember;
        Sykj.Web.Api.v1.MessageController _messageController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Message()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _siteMessage = _serviceProvider.GetService<ISiteMessage>();
            _users = _serviceProvider.GetService<IUsers>();
            _userMember = _serviceProvider.GetService<IUserMember>();
            _messageController = new Web.Api.v1.MessageController(_siteMessage, _users, _userMember);
        }

        /// <summary>
        /// 消息分类
        /// </summary>
        [Fact]
        public void MessageType()
        {
            _messageController.UserId = 88;
            ApiResult result = _messageController.MessageType();
            Assert.True(result.success);
        }

        /// <summary>
        /// 用户消息列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="msgType"></param>
        [Theory]
        [InlineData(1, 10, EnumHelper.MsgType.System)]
        public void MessageList(int pageIndex, int pageSize, EnumHelper.MsgType msgType)
        {
            _messageController.UserId = 88;
            ApiResult result = _messageController.MessageList(pageIndex, pageSize, msgType.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 用户消息设为已读
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="msgType"></param>
        [Theory]
        [InlineData(EnumHelper.MsgType.System)]
        public void MessageRead(EnumHelper.MsgType msgType)
        {
            _messageController.UserId = 88;
            ApiResult result = _messageController.MessageRead(msgType.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 未读消息条数
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="msgType"></param>
        [Fact]
        public void MessageCount()
        {
            _messageController.UserId = 88;
            ApiResult result = _messageController.MessageCount();
            Assert.True(result.success);
        }

        /// <summary>
        /// 一对一聊天列表 lh
        /// </summary>
        /// <param name="otherUserID">聊天对象的ID</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Theory]
        [InlineData(88, 1, 10)]
        public void MessageListChat(int otherUserID, int pageIndex = 1, int pageSize = 10)
        {
            _messageController.UserId = 3;
            ApiResult result = _messageController.MessageListChat(otherUserID, pageIndex, pageSize);
            Assert.True(result.success);
        }

        /// <summary>
        /// 发送消息 lh
        /// </summary>
        /// <param name="receiverId">接收者id</param>
        /// <param name="contentMsg">内容</param>
        /// <param name="contentType">内容类型</param>
        /// <returns></returns>
        [Theory]
        [InlineData(88, "单元测试消息", "")]
        public void MessageSend(int receiverId, string contentMsg, string contentType)
        {
            _messageController.UserId = 3;
            ApiResult result = _messageController.MessageSend(receiverId, contentMsg, contentType);
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除数据
        /// </summary>
        public void Dispose()
        {
            //清除数据
        }
    }
}
