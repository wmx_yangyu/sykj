using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.Components;
using Sykj.Infrastructure;
using System.Collections.Generic;
using Sykj.IServices;
using System.IO;
using System.Text;
using System.Net;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Sykj.Test
{
    public class UnitTest
    {
        IServiceProvider _serviceProvider;
        ICacheService _cacheService;
        ILog _log;
        IRegions _regions;
        ISiteMessage _siteMessage;

        public UnitTest()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _log = _serviceProvider.GetService<ILog>();
            _regions = _serviceProvider.GetService<IRegions>();
            _siteMessage = _serviceProvider.GetService<ISiteMessage>();
        }

        [Fact]
        public void UpdateSite()
        {
            _siteMessage.UpdateIsRead("Order", 674);
        }

        /// <summary>
        /// 测试支付宝签名
        /// </summary>
        [Fact]
        public void Test()
        {
            
        }

        [Fact]
        public void Hash()
        {
            string name1 = "liuxiang";
            string result1 = HashEncrypt.GetMd5(name1);

            string name2 = "liuxiang";
            string result2 = HashEncrypt.GetSHA1(name2);

            string name3 = "liuxiang";
            string result3 = HashEncrypt.GetSHA256(name3);
        }

        /// <summary>
        /// 测试绑定用户
        /// </summary>
        [Fact]
        public void BindUser()
        {
            string appId = "UKdtqcsX9GADGw11IBwPE6";
            var url = $"https://restapi.getui.com/v1/{appId}/bind_alias";
            var authToken = GetToken();
            var data = new JObject();
            JArray arr = new JArray();

            var obj = new JObject();
            obj.Add("cid", "8becdb11193b9ab61ec6914e36e3025f");
            obj.Add("alias", "277");
            arr.Add(obj);

            data.Add("alias_list", arr);
            HttpHelper httpHelper = new HttpHelper(url, "application/json");
            httpHelper.RequestHeaders.Add("authtoken", authToken);
            string result = httpHelper.SendPost(data.ToString());
        }

        /// <summary>
        /// 测试保存消息共同体
        /// </summary>
        [Fact]
        public void SaveListBody()
        {
            var appKey = "iidq6Mp2JH85txCMOTGWu9";
            string appId = "UKdtqcsX9GADGw11IBwPE6";
            var url = $"https://restapi.getui.com/v1/{appId}/save_list_body";
            var authToken = GetToken();
            var data = new JObject();
            JArray arr = new JArray();

            var msgObj = new JObject();
            msgObj.Add("appkey", appKey);
            msgObj.Add("is_offline", true);
            msgObj.Add("offline_expire_time", 10000000);
            msgObj.Add("msgtype", "notification");

            var notifiObj = new JObject();
            var styleObj = new JObject();
            styleObj.Add("type", 0);
            styleObj.Add("text", "推送内容");
            styleObj.Add("title", "这是标题");

            notifiObj.Add("style", styleObj);
            notifiObj.Add("transmission_type", true);
            notifiObj.Add("transmission_content", "透传内容，这里是不是详细内容");

            data.Add("message", msgObj);
            data.Add("notification", notifiObj);

            HttpHelper httpHelper = new HttpHelper(url, "application/json");
            httpHelper.RequestHeaders.Add("authtoken", authToken);
            string result = httpHelper.SendPost(data.ToString());
            JObject json = JObject.Parse(result);

            if (json["result"].ToString() == "ok")
            {
                var taskId = json["taskid"].ToString();
                var dd = SendToList(new List<string> { "277" }, taskId);
            }
            else
            {

            }
        }

        /// <summary>
        /// 测试消息群发送
        /// </summary>
        public static string SendToList(List<string> userIds, string taskId)
        {
            string appId = "UKdtqcsX9GADGw11IBwPE6";
            var url = $" https://restapi.getui.com/v1/{appId}/push_list";
            var authToken = GetToken();
            var data = new JObject();


            data.Add("alias", JArray.FromObject(userIds));
            data.Add("taskid", taskId);
            data.Add("need_detail", true);

            HttpHelper httpHelper = new HttpHelper(url, "application/json");
            httpHelper.RequestHeaders.Add("authtoken", authToken);
            string result = httpHelper.SendPost(data.ToString());
            return result;
        }

        public static string GetToken()
        {
            var appKey = "iidq6Mp2JH85txCMOTGWu9";
            string appId = "UKdtqcsX9GADGw11IBwPE6";
            string mastersecret = "kmWYpz67jD9qZaNHxl50A7";
            string timestamp = Utils.GetTimeStampms();
            var url = string.Format("https://restapi.getui.com/v1/{0}/auth_sign", appId);
            var data = new JObject();
            string sign = HashEncrypt.GetSHA256(appKey + timestamp + mastersecret);
            data.Add("sign", sign);
            data.Add("timestamp", timestamp);
            data.Add("appkey", appKey);
            HttpHelper httpHelper = new HttpHelper(url, "application/json");
            string result = httpHelper.SendPost(data.ToString());
            JObject json = JObject.Parse(result);

            if (json["result"].ToString() == "ok")
            {
                return json["auth_token"].ToString();
            }
            else
            {
                return json["result"].ToString();
            }
        }
        /// <summary>
        /// 测试用户注册
        /// </summary>
        [Fact]
        public void RegisterController()
        {
            IServices.IUserMember _userMember = _serviceProvider.GetService<IServices.IUserMember>();
            var model = _userMember.GetMemberModel(c => c.UserId == 58);
        }

        /// <summary>
        /// 测试发短信
        /// </summary>
        [Fact]
        public void TestSendSms()
        {
            ICacheService _cacheService = _serviceProvider.GetService<ICacheService>();

            Random rnd = new Random();
            int rand = rnd.Next(1000, 9999);
            string content = string.Format(BaseConfig.GetValue("SmsServerCalssName"), rand);

            string phone = "13378808354";
            string key = phone + "SMSCode";
            _cacheService.SetCache(key, rand, DateTime.Now.AddMinutes(2));

            string msg = string.Empty;
            ISmsServer smsServer = Components.Factory.CreateSmsServer();
            //bool b = smsServer.SendMsg("13378808354", "您好，您的验证码是{SMSCode}", ref msg);
        }

        /// 测试发短信
        /// </summary>
        [Fact]
        public void TestAES()
        {
            string iv = "1234567890000000";

            string s1 = AESEncrypt.Encrypt("liuxiang", "12345678900000001234567890000000", iv);
            string s = AESEncrypt.Decrypt2(s1, "12345678900000001234567890000000", iv);
        }

        [Fact]
        public void TestAES1()
        {
            string strURL = "http://swjy-edu.com/Data/Majors/GetByCode?code=081903";
            string result = string.Empty;
            try
            {
                HttpWebRequest wbRequest = (HttpWebRequest)WebRequest.Create(strURL);
                wbRequest.Method = "POST";
                HttpWebResponse wbResponse = (HttpWebResponse)wbRequest.GetResponse();
                using (Stream responseStream = wbResponse.GetResponseStream())
                {
                    using (StreamReader sReader = new StreamReader(responseStream))
                    {
                        result = sReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {

            }

            var result2 = System.Text.RegularExpressions.Regex.Unescape(result);//反转义
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void GetProvince()
        {
            string s = _regions.GetCityName("530102");
        }

        #region 压力测试 ydp
        [Fact]
        public void PressureTest()
        {
            for (var i = 0; i < 10000; i++)
            {
                Thread th = new Thread(HomeIndex);
                th.Start();
            }
        }

        private void HomeIndex()
        {
            var url = "https://www.yn5167.cn/api/v1/Home/homeindex";
            HttpHelper httpHelper = new HttpHelper(url);
            var result = httpHelper.SendGet();

        }
        #endregion 
    }
}
