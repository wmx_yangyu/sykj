﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Permissions : IDisposable
    {
        IServiceProvider _serviceProvider;
        IPermissions _permissions;
        PermissionsController _permissionsController;
        Sykj.Infrastructure.ICacheService _cacheService;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Permissions()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _permissions = _serviceProvider.GetService<IPermissions>();
            _cacheService = _serviceProvider.GetService<Sykj.Infrastructure.ICacheService>();
            _permissionsController = new PermissionsController(_permissions, _cacheService);
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="page">页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键词</param>
        /// <param name="type">类型</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,"",1)]
        public void List(int page, int limit, string keyWords, int type)
        {
            JsonResult r = _permissionsController.List(page, limit, keyWords, type) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            Sykj.Entity.Permissions model = new Entity.Permissions();
            model.ParentId = 0;
            model.Title = "测试权限";
            model.Description = "测试权限";
            model.Url = "测试权限";
            model.Icon = "测试权限";
            model.Sort = 1;
            model.IsEnable = true;
            model.Type = 1;
            _permissionsController.UserId = 3;
            JsonResult r = _permissionsController.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.ParentId = 0;
            model.Title = "测试权限";
            model.Description = "测试权限";
            model.Url = "测试权限";
            model.Icon = "测试权限";
            model.Sort = 1;
            model.IsEnable = true;
            model.Type = 1;
            r = _permissionsController.Edit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.PermissionId);
            r = _permissionsController.DeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result2.success&& result3.success);
        }

        public void Dispose()
        {
        }
    }
}
