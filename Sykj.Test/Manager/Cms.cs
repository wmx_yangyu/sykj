﻿using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 编码分组//编码明细测试====bjg
    /// </summary>
    public class Cms : IDisposable
    {
        private readonly IServiceProvider _serviceProvider;
        IModule _module;
        IChannel _channel;
        IContent _content;
        CmsController _cms;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Cms()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _module = _serviceProvider.GetService<IModule>();
            _channel = _serviceProvider.GetService<IChannel>();
            _content = _serviceProvider.GetService<IContent>();
            _cms = new CmsController(_module, _channel, _content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyWords">关键词</param>
        [Theory]
        [InlineData("")]
        public void ChannelList(string keyWords)
        {
            JsonResult r = _cms.ChannelList(keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        int id = 0;
        /// <summary>
        /// 添加测试
        /// </summary>
        [Fact]
        public void ChannelAdd()
        {
            Sykj.Entity.Channel model = new Entity.Channel()
            {
                ChanneName = "测试栏目",
                Sort = 3,
                Status = 0,
                ModuleId =1,
                CreateUserId = 88,
                ParentId = 0
            };
            JsonResult r = _cms.ChannelAdd(model) as JsonResult;
            id = model.ChannelId;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 添加测试
        /// </summary>
        [Fact]
        public void ChannelEdit()
        {
            Sykj.Entity.Channel model = new Entity.Channel()
            {
                ChannelId = id,
                ChanneName = "测试栏目修改",
                Sort = 3,
                Status = 0,
                ModuleId = 1,
                CreateUserId = 88,
                ParentId = 0
            };
            JsonResult r = _cms.ChannelEdit(model) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 文章列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <param name="channelId"></param>
        /// <param name="sortField"></param>
        /// <param name="sortType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,"咨询",34,"Sort","DESC","0")]
        public void ContentList(int page, int limit, string keyWords,
            int channelId, string sortField, string sortType, string status)
        {
            //JsonResult r = _cms.ContentList(page, limit, keyWords, "", "","" ,sortType, status) as JsonResult;
            //ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void GetChannelTreeList()
        {
            JsonResult r = _cms.GetChannelTreeList() as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentDel()
        {
            JsonResult r = _cms.ContentDel(id) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        int contentId = 0;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentAdd()
        {
            Sykj.Entity.Content content = new Entity.Content()
            {
                Title = "测试",
                CreateUserId = 88,
                PvCount = 0,
                ChannelId = 34,
                Sort = 1,
                IsRecomend = true,
                IsHot = true,
                IsColor = true,
                IsTop = true,
                TotalComment = 0,
                TotalSupport = 0,
                TotalFav = 0,
                TotalShare = 0
            };
            JsonResult r = _cms.ContentAdd(content) as JsonResult;
            contentId = content.ChannelId;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentEdit()
        {
            Sykj.Entity.Content content = new Entity.Content()
            {
                ContentId=34,
                Title = "测试",
                CreateUserId = 88,
                PvCount = 0,
                ChannelId = 34,
                Sort = 1,
                IsRecomend = true,
                IsHot = true,
                IsColor = true,
                IsTop = true,
                TotalComment = 0,
                TotalSupport = 0,
                TotalFav = 0,
                TotalShare = 0
            };
            JsonResult r = _cms.ContentEdit(content) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentModify()
        {
            JsonResult r = _cms.ContentModify(contentId,1,"Sort") as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentAudit()
        {
            List<int> ids = new List<int>();
            ids.Add(contentId);
            JsonResult r = _cms.ContentAudit(ids) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ContentNoAudit()
        {
            List<int> ids = new List<int>();
            ids.Add(contentId);
            JsonResult r = _cms.ContentNoAudit(ids) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void DeleteAll()
        {
            List<int> ids = new List<int>();
            ids.Add(contentId);
            JsonResult r = _cms.DeleteAll(ids) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 数据清除
        /// </summary>
        public void Dispose()
        {
        }
    }
}
