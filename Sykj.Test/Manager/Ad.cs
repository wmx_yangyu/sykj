﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;

namespace Sykj.Test.Manager
{
    public class Ad : IDisposable
    {
        IServiceProvider _serviceProvider;
        IAdvertisement _advertisement;
        IAdvertiseposition _advertiseposition;
        Sykj.Infrastructure.ICacheService _cacheService;
        AdController _adController;

        /// <summary>
        /// 构造函数
        /// </summary>
        public Ad()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _advertisement = _serviceProvider.GetService<IAdvertisement>();
            _advertiseposition = _serviceProvider.GetService<IAdvertiseposition>();
            _cacheService = _serviceProvider.GetService<Sykj.Infrastructure.ICacheService>();
            _adController = new AdController(_advertisement, _advertiseposition, _cacheService);
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,null)]
        public void AdpositionList(int page, int limit, string keyWords)
        {
            JsonResult r = _adController.AdpositionList(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删
        /// </summary>
        [Fact]
        public void AddAndDelete()
        {
            //广告位增
            Sykj.Entity.Advertiseposition model = new Entity.Advertiseposition()
            {
                AdvPositionName = "测试广告位",
                ShowType = 1,
                RepeatColumns = 1,
            };
            _adController.UserId = 3;
            JsonResult r = _adController.AdpositionAdd(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //广告位改
            model.AdvPositionName = "测试广告位";
            model.ShowType = 1;
            model.RepeatColumns = 1;
            r = _adController.AdpositionEdit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //内容增
            Sykj.Entity.Advertisement advertisementModel = new Entity.Advertisement()
            {
                AdvertisementName = "测试广告内容",
                AdvPositionId = model.AdvPositionId,
                ContentType = 1
            };
            r = _adController.AdvertisementAdd(advertisementModel) as JsonResult;
            ApiResult result4 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //内容改
            advertisementModel.AdvertisementName = "测试广告内容";
            advertisementModel.AdvPositionId = model.AdvPositionId;
            advertisementModel.ContentType = 2;
            r = _adController.AdvertisementEdit(advertisementModel) as JsonResult;
            ApiResult result5 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //内容删
            List<int> ids2 = new List<int>();
            ids2.Add(advertisementModel.AdvertisementId);
            r = _adController.AdvertisementDeleteAll(ids2) as JsonResult;
            ApiResult result6 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            //广告位删
            List<int> ids = new List<int>();
            ids.Add(model.AdvPositionId);
            r = _adController.AdpositionDeleteAll(ids) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result2.success && result3.success && result4.success && result5.success && result6.success);
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="limit">每页数据条数</param>
        /// <param name="keyWords">搜索关键字</param>
        /// <param name="advPositionId">广告位id</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1, 10, null,79)]
        public void AdvertisementList(int page, int limit, string keyWords, int advPositionId)
        {
            JsonResult r = _adController.AdvertisementList(page, limit, keyWords, advPositionId) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除测试数据
        /// </summary>
        public void Dispose()
        {
        }
    }
}
