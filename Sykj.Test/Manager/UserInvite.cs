﻿using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;

namespace Sykj.Test.Manager
{
    public class UserInvite : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUserinvite _userinvite;
        UserinviteController userinvite;

        /// <summary>
        /// 构造方法=====bjg
        /// </summary>
        public UserInvite()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _userinvite = _serviceProvider.GetService<IUserinvite>();
            userinvite = new UserinviteController(_userinvite);
        }

        /// <summary>
        /// 查询邀请人
        /// </summary>
        /// <param name="page">起始页</param>
        /// <param name="size">页大小</param>
        /// <param name="major">专业名称</param>
        /// <param name="collegeId">高校Id</param>
        /// <param name="status">是否是特色专业</param>
        [Theory]
        [InlineData(1, 10, "")]
        [InlineData(2, 10, "")]
        public void List(int page, int size, string keyWords)
        {
            JsonResult r = userinvite.List(page, size, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            Sykj.Entity.Userinvite model = new Entity.Userinvite();
            model.UserId = 19;
            model.UserNick = "11000";
            model.InviteUserId = 145;
            model.Depth = 1;
            model.Path = "";
            model.Status = 1;
            model.IsRebate = true;
            model.IsNew = true;
            model.CreatedDate = DateTime.Now;
            model.RebateDesc = "111";
            model.InviteType = 1;
            userinvite.UserId = 88;
            JsonResult r = userinvite.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.UserId = 19;
            model.UserNick = "11000";
            model.InviteUserId = 145;
            model.Depth = 1;
            model.Path = "";
            model.Status = 1;
            model.IsRebate = true;
            model.IsNew = true;
            model.CreatedDate = DateTime.Now;
            model.RebateDesc = "111";
            model.InviteType = 1;
            r = userinvite.Edit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.Id);
            r = userinvite.DeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success && result2.success&& result3.success);
        }

        /// <summary>
        /// 清除数据
        /// </summary>
        public void Dispose()
        {
            //
        }

    }
}
