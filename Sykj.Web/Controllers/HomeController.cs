﻿
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Web.Controllers
{
    /// <summary>
    /// 默认首页
    /// </summary>
    public class HomeController : BaseController
    {
        /// <summary>
        /// 处理请求
        /// </summary>
        public IActionResult Index()
        {
            return Redirect("/Api/v1/Home");
        }

        /// <summary>
        /// 下载
        /// </summary>
        /// <returns></returns>
        public IActionResult DownLoad()
        {
            string client = Request.GetUserAgent().ToLower();
            string url = "";
            if (client.IndexOf("iphone") > 0)
            {
                url = "https://a.app.qq.com/o/simple.jsp?pkgname=cn.sykjwh.ytgkzy";
            }
            else if (client.IndexOf("android") > 0)
            {
                url = "https://a.app.qq.com/o/simple.jsp?pkgname=cn.sykjwh.ytgkzy";

            }
            else
            {
                url = "https://a.app.qq.com/o/simple.jsp?pkgname=cn.sykjwh.ytgkzy";
            }
            return Redirect(url);
        }
    }
}