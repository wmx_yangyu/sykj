﻿
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;
using Sykj.ViewModel;

namespace Sykj.Web.Api.v1
{
    /// <summary>
    /// (查数据)主页
    /// </summary>
    public class HomeController : ApiController
    {
        #region 全局变量
        private readonly IAdvertisement _advertisement;
        private readonly IRegions _newRegions;
        private readonly IContent _content;
        private readonly IUsers _users;
        private readonly IUserMember _userMember;
        #endregion

        #region 构造方法
        /// <summary>
        /// 构造方法
        /// </summary>
        public HomeController(IAdvertisement advertisement, IRegions newRegions, IContent content, IUsers users, IUserMember userMember)
        {
            _advertisement = advertisement;
            _newRegions = newRegions;
            _content = content;
            _users = users;
            _userMember = userMember;
        }
        #endregion

        #region 首页—首页广告位|推荐高校|咨询广告位 bjg
        /// <summary>
        /// 首页广告位//推荐高校//咨询广告位 bjg
        /// </summary>
        /// <returns></returns>
        [HttpGet("homeindex")]
        public ApiResult HomeIndex()
        {
            dynamic data = new System.Dynamic.ExpandoObject();
            //首页广告
            var selectAdsList = _advertisement.GetAdPostion(78);

            selectAdsList.ForEach(c =>
            {
                c.FileUrl = (!string.IsNullOrWhiteSpace(c.FileUrl)) ? (c.FileUrl.StartsWith("http") ? c.FileUrl : BaseConfig.ImagesDoMain + c.FileUrl) : BaseConfig.ImagesDoMain + "/upload/shop/test/default.jpg";
            });
            data.SelectAdsList = selectAdsList;
            //咨询广告
            var consultAdsList = _advertisement.GetAdPostion(79);
            consultAdsList.ForEach(c =>
            {
                c.FileUrl = (!string.IsNullOrWhiteSpace(c.FileUrl)) ? (c.FileUrl.StartsWith("http") ? c.FileUrl : BaseConfig.ImagesDoMain + c.FileUrl) : BaseConfig.ImagesDoMain + "/upload/shop/test/default.jpg";
            });
            data.ConsultAdsList = consultAdsList;
            var noticeS = _content.GetList(5, "ChannelID=@0", "ContentID DESC", 40).Select(c => new
            {
                c.ContentId,
                c.Title
            });

            //滚动公告
            data.NoticeList = noticeS;
            return DataResult(data);
        }
        #endregion

    }
}