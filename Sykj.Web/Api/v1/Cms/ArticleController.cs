﻿
using Microsoft.AspNetCore.Mvc;
using Sykj.ViewModel;
using Sykj.IServices;
using Sykj.Components;
using System.Linq;

namespace Sykj.Web.Api.v1
{
    /// <summary>
    /// CMS管理
    /// </summary>
    public class ArticleController : ApiController
    {
        private readonly IContent _content;

        #region 构造方法
        /// <summary>
        /// 构造方法
        /// </summary>
        public ArticleController(IContent content)
        {
            _content = content;
        }
        #endregion

        #region CMS—文章列表   bjg
        /// <summary>
        /// CMS_文章列表   bjg
        /// </summary>
        /// <param name="top">条数</param>
        /// <param name="classId">栏目Id</param>
        /// <returns></returns>
        [HttpGet("list")]
        public ApiResult List(int top, int classId)
        {
            var list = _content.GetList(top, "ChannelID=@0", "ContentID DESC", classId).ToList();
            var resultList = list.Select(c => new
            {
                c.ContentId,
                c.Title,
                imageUrl = string.IsNullOrWhiteSpace(c.ImageUrl) ? "" : c.ImageUrl.StartsWith("http") ? c.ImageUrl : BaseConfig.ImagesDoMain + c.ImageUrl,
                linkUrl = string.IsNullOrWhiteSpace(c.LinkUrl) ? "" : c.LinkUrl.StartsWith("http") ? c.LinkUrl : BaseConfig.ImagesDoMain + c.LinkUrl,
                thumbImageUrl = string.IsNullOrWhiteSpace(c.ThumbImageUrl) ? "" : c.ThumbImageUrl.StartsWith("http") ? c.ThumbImageUrl : BaseConfig.ImagesDoMain + c.ThumbImageUrl,
                c.Type,
                c.CreateDate
            });
            return DataResult(resultList);
        }
        #endregion

        #region CMS—文章详情 bjg
        /// <summary>
        /// CMS—文章详情 bjg
        /// </summary>
        /// <param name="id">文章id</param>
        /// <returns></returns>
        [HttpGet("detail")]
        public ApiResult Detail(int id)
        {
            var model = _content.GetModel(x => x.ContentId == id);
            if (model == null)
            {
                return Failed("文章不存在！");
            }
            model.ImageUrl = string.IsNullOrWhiteSpace(model.ImageUrl) ? "" : model.ImageUrl.StartsWith("http") ? model.ImageUrl : BaseConfig.ImagesDoMain + model.ImageUrl;
            model.LinkUrl = string.IsNullOrWhiteSpace(model.LinkUrl) ? "" : model.LinkUrl.StartsWith("http") ? model.LinkUrl : BaseConfig.ImagesDoMain + model.LinkUrl;
            model.ThumbImageUrl = string.IsNullOrWhiteSpace(model.ThumbImageUrl) ? "" : model.ThumbImageUrl.StartsWith("http") ? model.ThumbImageUrl : BaseConfig.ImagesDoMain + model.ThumbImageUrl;
            model.Description = string.IsNullOrWhiteSpace(model.Description) ? "" : model.Description.Replace("/upload/", $"{BaseConfig.DoMain}/upload/");
            return DataResult(model);
        }
        #endregion

        #region CMS—文章列表(分页) bjg
        /// <summary>
        /// CMS—文章列表(分页)
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="classId">栏目Id</param>
        /// <returns></returns>
        [HttpGet("pagelist")]
        public ApiResult PageList(int pageIndex, int pageSize, int classId)
        {
            var list = _content.GetPagedList(pageIndex, pageSize, "ChannelID=@0", "ContentID DESC", classId).ToList();
            var resultList = list.Select(c => new
            {
                c.ContentId,
                c.Title,
                imageUrl = string.IsNullOrWhiteSpace(c.ImageUrl) ? "" : c.ImageUrl.StartsWith("http") ? c.ImageUrl : BaseConfig.ImagesDoMain + c.ImageUrl,
                linkUrl = string.IsNullOrWhiteSpace(c.LinkUrl) ? "" : c.LinkUrl.StartsWith("http") ? c.LinkUrl : BaseConfig.ImagesDoMain + c.LinkUrl,
                thumbImageUrl = string.IsNullOrWhiteSpace(c.ThumbImageUrl) ? "" : c.ThumbImageUrl.StartsWith("http") ? c.ThumbImageUrl : BaseConfig.ImagesDoMain + c.ThumbImageUrl,
                c.Type,
                c.CreateDate
            });
            return DataResult(resultList);
        }
        #endregion
    }
}