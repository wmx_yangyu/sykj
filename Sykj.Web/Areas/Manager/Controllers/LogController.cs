﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.Infrastructure;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LogController : MController
    {
        ILog _log;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public LogController(ILog log)
        {
            _log = log;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="page">起始页</param>
        /// <param name="limit">页大小</param>
        /// <param name="type">日志类型</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="keyWords">关键词</param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, int type, string startTime, string endTime, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (Title.Contains(@0) OR Detail.Contains(@0))");
            }
            if (!string.IsNullOrWhiteSpace(startTime))
            {
                strWhere.Append("AND CreateDate>=@1 ");
            }
            if (!string.IsNullOrWhiteSpace(endTime))
            {
                strWhere.Append(" AND CreateDate<@2 ");
            }
            if (type >0 )
            {
                strWhere.Append(" AND Type=@3 ");
            }
            var list = _log.GetPagedList(page, limit, strWhere.ToString(), " logId desc ", keyWords,startTime,endTime,type);
            int recordCount = _log.GetRecordCount(strWhere.ToString(), keyWords, startTime, endTime, type);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 导出excel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ExportList()
        {
            var query = _log.GetList().Select(c => new
            {
                日志标题 = c.Title,
                日志信息 = c.Detail,
                日志类型=c.Type,
                创建时间 = c.CreateDate
            });
            var list = query.ToList();
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
            AsposeExcel asposeExcel = new AsposeExcel();
            byte[] buffer = asposeExcel.Export(list);
            return File(buffer, "application/ms-excel", fileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _log.GetList(c => ids.Contains(c.LogId));
            bool b = _log.Delete(list);
            Components.LogOperate.Add("批量删除日志", "删除ID为【" + string.Join(",", ids) + "】的日志", HttpContext);
            return Json(AjaxResult(b));
        }

    }
}