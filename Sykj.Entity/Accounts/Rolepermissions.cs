﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 角色对应权限
    /// </summary>
    public class Rolepermissions
    {
        public int Id { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; set; }
        /// <summary>
        /// 权限url
        /// </summary>
        [NotMapped]//不参与EF映射
        public string Url { get; set; }
    }
}
