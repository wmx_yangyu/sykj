﻿

namespace Sykj.Entity
{
    /// <summary>
    /// 用户与角色
    /// </summary>
    public class UserRoles
    {
        public int Id { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }
    }
}
