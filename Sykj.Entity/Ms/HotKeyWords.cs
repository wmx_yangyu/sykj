/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-18 15:14:22 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Hotkeywords                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 
	/// </summary>
	public class HotKeyWords
	{
		/// <summary>
		/// 主键
		/// </summary>
		public Int32 Id {get;set;}

		/// <summary>
		/// 关键字
		/// </summary>
		[Required(ErrorMessage = "请输入关键字")]
		[MaxLength(255)]
		public String KeyWords {get;set;}

		/// <summary>
		/// 分类Id
		/// </summary>
		[Required(ErrorMessage = "请输入分类Id")]
		public Int32 CategoryId {get;set;}

        /// <summary>
        /// 分类名
        /// </summary>
        [NotMapped]
        public string CateName { get; set; }
	}
}
