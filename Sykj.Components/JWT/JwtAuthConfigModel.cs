﻿using System;

namespace Sykj.Components
{
    /// <summary>
    /// Jwt配置读取
    /// </summary>
    public class JwtAuthConfigModel 
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public JwtAuthConfigModel()
        {
            try
            {
                JWTSecretKey = BaseConfig.Configuration["JwtAuth:SecurityKey"];
                WebExp = double.Parse(BaseConfig.Configuration["JwtAuth:WebExp"]);
                AppExp = double.Parse(BaseConfig.Configuration["JwtAuth:AppExp"]);
                MiniProgramExp = double.Parse(BaseConfig.Configuration["JwtAuth:MiniProgramExp"]);
                OtherExp = double.Parse(BaseConfig.Configuration["JwtAuth:OtherExp"]);
                Issuer = BaseConfig.Configuration["JwtAuth:Issuer"];
                Audience = BaseConfig.Configuration["JwtAuth:Audience"];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// JWT加密key 不低于16个字符
        /// </summary>
        public string JWTSecretKey;
        /// <summary>
        /// 颁发给web的token过期时间
        /// </summary>
        public double WebExp;
        /// <summary>
        /// 颁发给app的token过期时间
        /// </summary>
        public double AppExp;
        /// <summary>
        /// 颁发给小程序的token过期时间
        /// </summary>
        public double MiniProgramExp;
        /// <summary>
        /// 其他的过期时间
        /// </summary>
        public double OtherExp = 12;
        /// <summary>
        /// 颁发者
        /// </summary>
        public string Issuer;
        /// <summary>
        /// 颁发给谁
        /// </summary>
        public string Audience;
    }
}
