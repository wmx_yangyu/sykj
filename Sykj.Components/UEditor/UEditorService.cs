﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace Sykj.Components
{
    /// <summary>
    /// 百度编辑器服务
    /// </summary>
    public class UEditorService
    {
        private UEditorActionCollection actionList;

        public UEditorService(IHostingEnvironment env, UEditorActionCollection actions)
        {
            Config.WebRootPath = env.WebRootPath;
            actionList = actions;
        }

        public void DoAction(HttpContext context)
        {
            var action = context.Request.Query["action"];
            if (action.Count<=0)
            {
                new NotSupportedHandler(context).Process();
                return;
            }
                
            if (actionList.ContainsKey(action))
                actionList[action].Invoke(context);
            else
                new NotSupportedHandler(context).Process();
        }
    }
}
