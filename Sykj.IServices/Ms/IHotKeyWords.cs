/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-18 15:14:22     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： IHotkeywords                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 
	/// </summary>
    public interface IHotKeyWords: Sykj.Repository.IRepository<Sykj.Entity.HotKeyWords>
    {
	   
    }
}