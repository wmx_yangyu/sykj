/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-08 17:30:16     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： ISitemessage                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 
	/// </summary>
    public interface ISiteMessage: Sykj.Repository.IRepository<Sykj.Entity.SiteMessage>
    {
        /// <summary>
        /// 设置某个用户某类消息为已读
        /// </summary>
        /// <param name="msgType">消息类型</param>
        /// <param name="receiverId">接收人</param>
        /// <returns></returns>
        bool UpdateIsRead(string msgType, int receiverId);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(Sykj.Entity.SiteMessage model, ref string errorMsg);
    }
}