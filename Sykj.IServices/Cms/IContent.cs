﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 文章内容
    /// </summary>
    public interface IContent : Sykj.Repository.IRepository<Sykj.Entity.Content>
    {
        /// <summary>
        /// 根据栏目ID获取指定条数记录
        /// </summary>
        /// <param name="channelId">栏目ID</param>
        /// <param name="top"></param>
        /// <param name="isDesc">是否倒序</param>
        /// <returns></returns>
        List<Sykj.Entity.Content> GetListCid(int channelId, int top = 0, bool isDesc = true);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        new IQueryable<Sykj.Entity.Content> GetPagedList(int pageIndex,
            int pageSize, string predicate, string ordering, params object[] args);
    }
}
