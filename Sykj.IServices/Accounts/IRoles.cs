﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoles : Sykj.Repository.IRepository<Sykj.Entity.Roles>
    {
        /// <summary>
        /// 组装树
        /// </summary>
        /// <returns></returns>
        List<Sykj.ViewModel.ZTreeModel> GetTreeList();

        /// <summary>
        /// 根据角色ID获取权限列表
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        List<Sykj.Entity.Permissions> GetPermissionsList(int roleId);

        /// <summary>
        /// 为角色分配权限
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        bool AddPermissions(List<Sykj.Entity.Rolepermissions> list);

        /// <summary>
        /// 清空该角色下的所有权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        bool DeletePermissions(int roleId);

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        bool DeleteAll(List<int> ids);
    }

}
