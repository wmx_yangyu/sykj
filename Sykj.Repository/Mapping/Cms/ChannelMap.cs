﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ChannelMap : IEntityTypeConfiguration<Sykj.Entity.Channel>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Channel> entity)
        {
            entity.HasKey(e => e.ChannelId);

            entity.ToTable("cms_channel");
        }
    }
}
