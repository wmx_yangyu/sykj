/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：积分明细                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-06 14:26:00 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Pointsdetail                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 积分明细
	/// </summary>
	public class PointsDetailMap : IEntityTypeConfiguration<Sykj.Entity.PointsDetail>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.PointsDetail> entity)
        {
            entity.HasKey(e => e.PointId);

            entity.ToTable("accounts_pointsdetail");
        }
    }
}
