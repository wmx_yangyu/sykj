using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 用户扩展表
	/// </summary>
	public class UsersexpMap : IEntityTypeConfiguration<Sykj.Entity.Usersexp>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Usersexp> entity)
        {
            entity.HasKey(e => e.UserId);

            entity.ToTable("accounts_usersexp");
        }
    }
}
