﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ConfigsysMap : IEntityTypeConfiguration<Sykj.Entity.Configsys>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Configsys> entity)
        {
            entity.HasKey(e => e.CsId);

            entity.ToTable("ms_configsys");
        }
    }
}
