﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ThumbnailSizeMap : IEntityTypeConfiguration<Sykj.Entity.ThumbnailSize>
    {
        public void Configure(EntityTypeBuilder<Entity.ThumbnailSize> entity)
        {
            entity.HasKey(e => e.ThumId);

            entity.ToTable("ms_thumbnailsize");
        }
    }
}
