﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sykj.Repository.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "accounts_permissions",
                columns: table => new
                {
                    PermissionId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: false),
                    Icon = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: false),
                    Path = table.Column<string>(nullable: true),
                    Depth = table.Column<int>(nullable: false),
                    Sort = table.Column<int>(nullable: false),
                    IsEnable = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_permissions", x => x.PermissionId);
                });

            migrationBuilder.CreateTable(
                name: "accounts_pointsdetail",
                columns: table => new
                {
                    PointId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RuleId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Score = table.Column<int>(nullable: false),
                    CurrentPoints = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_pointsdetail", x => x.PointId);
                });

            migrationBuilder.CreateTable(
                name: "accounts_rolepermissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    PermissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_rolepermissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "accounts_roles",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "accounts_userroles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_userroles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "accounts_users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 100, nullable: false),
                    Password = table.Column<string>(maxLength: 100, nullable: false),
                    NickName = table.Column<string>(maxLength: 100, nullable: true),
                    TrueName = table.Column<string>(maxLength: 100, nullable: true),
                    Sex = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 100, nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    DepartmentId = table.Column<string>(maxLength: 100, nullable: true),
                    Activity = table.Column<bool>(nullable: false),
                    UserType = table.Column<string>(maxLength: 50, nullable: true),
                    Creator = table.Column<int>(nullable: true),
                    Gravatar = table.Column<string>(maxLength: 255, nullable: true),
                    WechatOpenId = table.Column<string>(maxLength: 100, nullable: true),
                    AppOpenId = table.Column<string>(maxLength: 100, nullable: true),
                    UnionId = table.Column<string>(maxLength: 100, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "accounts_usersexp",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Singature = table.Column<string>(maxLength: 200, nullable: true),
                    TelPhone = table.Column<string>(maxLength: 50, nullable: true),
                    QQ = table.Column<string>(maxLength: 50, nullable: true),
                    HomePage = table.Column<string>(maxLength: 50, nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    NativePlace = table.Column<string>(maxLength: 50, nullable: true),
                    RegionId = table.Column<string>(maxLength: 100, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    Grade = table.Column<int>(nullable: false),
                    Balance = table.Column<decimal>(nullable: true),
                    Points = table.Column<int>(nullable: true),
                    IsUserDPI = table.Column<bool>(nullable: true),
                    UserCardCode = table.Column<string>(maxLength: 50, nullable: true),
                    UserCardType = table.Column<int>(nullable: true),
                    SourceType = table.Column<int>(nullable: true),
                    CardId = table.Column<string>(maxLength: 100, nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    LastLoginDate = table.Column<DateTime>(nullable: true),
                    ExpireTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_usersexp", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "accounts_usertype",
                columns: table => new
                {
                    UserType = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts_usertype", x => x.UserType);
                });

            migrationBuilder.CreateTable(
                name: "ad_advertisement",
                columns: table => new
                {
                    AdvertisementId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AdvertisementName = table.Column<string>(nullable: false),
                    AdvPositionId = table.Column<int>(nullable: false),
                    ContentType = table.Column<int>(nullable: false),
                    FileUrl = table.Column<string>(nullable: true),
                    AlternateText = table.Column<string>(nullable: true),
                    NavigateUrl = table.Column<string>(nullable: true),
                    AdvHtml = table.Column<string>(nullable: true),
                    Impressions = table.Column<int>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateUserId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    DayMaxPv = table.Column<int>(nullable: true),
                    DayMaxIp = table.Column<int>(nullable: true),
                    CpmPrice = table.Column<double>(nullable: true),
                    AutoStop = table.Column<int>(nullable: true),
                    Sort = table.Column<int>(nullable: true),
                    OperationType = table.Column<string>(nullable: true),
                    TargetId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ad_advertisement", x => x.AdvertisementId);
                });

            migrationBuilder.CreateTable(
                name: "ad_advertiseposition",
                columns: table => new
                {
                    AdvPositionId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AdvPositionName = table.Column<string>(nullable: false),
                    ShowType = table.Column<int>(nullable: false),
                    RepeatColumns = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: true),
                    Height = table.Column<int>(nullable: true),
                    AdvHtml = table.Column<string>(nullable: true),
                    IsOne = table.Column<bool>(nullable: false),
                    TimeInterval = table.Column<int>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ad_advertiseposition", x => x.AdvPositionId);
                });

            migrationBuilder.CreateTable(
                name: "cms_channel",
                columns: table => new
                {
                    ChannelId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ChanneName = table.Column<string>(nullable: false),
                    Sort = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Keywords = table.Column<string>(nullable: true),
                    ModuleId = table.Column<int>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateUserId = table.Column<int>(nullable: false),
                    Path = table.Column<string>(nullable: true),
                    Depth = table.Column<int>(nullable: true),
                    IsHasChildren = table.Column<bool>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeywords = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_channel", x => x.ChannelId);
                });

            migrationBuilder.CreateTable(
                name: "cms_content",
                columns: table => new
                {
                    ContentId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: false),
                    SubTitle = table.Column<string>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    ThumbImageUrl = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateUserId = table.Column<int>(nullable: false),
                    LastEditUserId = table.Column<int>(nullable: true),
                    LastEditDate = table.Column<DateTime>(nullable: true),
                    LinkUrl = table.Column<string>(nullable: true),
                    PvCount = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ChannelId = table.Column<int>(nullable: false),
                    Keywords = table.Column<string>(nullable: true),
                    Sort = table.Column<int>(nullable: false),
                    IsRecomend = table.Column<bool>(nullable: false),
                    IsHot = table.Column<bool>(nullable: false),
                    IsColor = table.Column<bool>(nullable: false),
                    IsTop = table.Column<bool>(nullable: false),
                    Remary = table.Column<string>(nullable: true),
                    TotalComment = table.Column<int>(nullable: false),
                    TotalSupport = table.Column<int>(nullable: false),
                    TotalFav = table.Column<int>(nullable: false),
                    TotalShare = table.Column<int>(nullable: false),
                    BeFrom = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeywords = table.Column<string>(nullable: true),
                    StaticUrl = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_content", x => x.ContentId);
                });

            migrationBuilder.CreateTable(
                name: "cms_module",
                columns: table => new
                {
                    ModuleId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_module", x => x.ModuleId);
                });

            migrationBuilder.CreateTable(
                name: "ms_configsys",
                columns: table => new
                {
                    CsId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    KeyName = table.Column<string>(nullable: false),
                    KeyValue = table.Column<string>(nullable: false),
                    KeyType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_configsys", x => x.CsId);
                });

            migrationBuilder.CreateTable(
                name: "ms_dic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    Value = table.Column<string>(maxLength: 100, nullable: false),
                    Types = table.Column<int>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    Remark = table.Column<string>(maxLength: 500, nullable: true),
                    Signs = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_dic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ms_favorite",
                columns: table => new
                {
                    FavoriteId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    TargetId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Tags = table.Column<string>(maxLength: 255, nullable: true),
                    Remark = table.Column<string>(maxLength: 255, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_favorite", x => x.FavoriteId);
                });

            migrationBuilder.CreateTable(
                name: "ms_guestbook",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    NickName = table.Column<string>(maxLength: 255, nullable: true),
                    Title = table.Column<string>(maxLength: 500, nullable: true),
                    Description = table.Column<string>(maxLength: 65535, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    HandlerNickName = table.Column<string>(maxLength: 255, nullable: true),
                    HandlerUserId = table.Column<int>(nullable: true),
                    HandlerDate = table.Column<DateTime>(nullable: true),
                    Privacy = table.Column<int>(nullable: false),
                    ReplyDescription = table.Column<string>(maxLength: 65535, nullable: true),
                    ParentId = table.Column<int>(nullable: false),
                    AuditStatus = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    BrowseCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_guestbook", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ms_hotkeywords",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    KeyWords = table.Column<string>(maxLength: 255, nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_hotkeywords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ms_images",
                columns: table => new
                {
                    ImageId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TargetId = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    ThumbnailUrl = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FileExt = table.Column<string>(nullable: true),
                    FileSize = table.Column<string>(nullable: true),
                    TableName = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_images", x => x.ImageId);
                });

            migrationBuilder.CreateTable(
                name: "ms_log",
                columns: table => new
                {
                    LogId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    IpAddress = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_log", x => x.LogId);
                });

            migrationBuilder.CreateTable(
                name: "ms_regions",
                columns: table => new
                {
                    AreaId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RegionId = table.Column<string>(nullable: true),
                    ParentId = table.Column<string>(nullable: true),
                    RegionName = table.Column<string>(nullable: true),
                    Spell = table.Column<string>(nullable: true),
                    Sort = table.Column<int>(nullable: false),
                    Path = table.Column<string>(nullable: true),
                    Depth = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_regions", x => x.AreaId);
                });

            migrationBuilder.CreateTable(
                name: "ms_sitemessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SenderID = table.Column<int>(nullable: true),
                    ReceiverID = table.Column<int>(nullable: true),
                    Title = table.Column<string>(maxLength: 300, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    MsgType = table.Column<string>(maxLength: 50, nullable: true),
                    SendTime = table.Column<DateTime>(nullable: false),
                    ReadTime = table.Column<DateTime>(nullable: true),
                    ReceiverIsRead = table.Column<bool>(nullable: true),
                    SenderIsDel = table.Column<bool>(nullable: true),
                    ReaderIsDel = table.Column<bool>(nullable: true),
                    Ext1 = table.Column<string>(maxLength: 300, nullable: true),
                    Ext2 = table.Column<string>(maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_sitemessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ms_thumbnailsize",
                columns: table => new
                {
                    ThumId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ThumName = table.Column<string>(nullable: true),
                    ThumWidth = table.Column<int>(nullable: false),
                    ThumHeight = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    ThumMode = table.Column<string>(nullable: true),
                    IsWatermark = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateUserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ms_thumbnailsize", x => x.ThumId);
                });

            migrationBuilder.CreateTable(
                name: "Userinvite",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    UserNick = table.Column<string>(maxLength: 255, nullable: false),
                    InviteUserId = table.Column<int>(nullable: false),
                    InviteNick = table.Column<string>(maxLength: 255, nullable: false),
                    Depth = table.Column<int>(nullable: false),
                    Path = table.Column<string>(maxLength: 255, nullable: false),
                    Status = table.Column<int>(nullable: false),
                    IsRebate = table.Column<bool>(nullable: false),
                    IsNew = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Remark = table.Column<string>(maxLength: 500, nullable: true),
                    RebateDesc = table.Column<string>(maxLength: 255, nullable: false),
                    InviteType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Userinvite", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "accounts_permissions");

            migrationBuilder.DropTable(
                name: "accounts_pointsdetail");

            migrationBuilder.DropTable(
                name: "accounts_rolepermissions");

            migrationBuilder.DropTable(
                name: "accounts_roles");

            migrationBuilder.DropTable(
                name: "accounts_userroles");

            migrationBuilder.DropTable(
                name: "accounts_users");

            migrationBuilder.DropTable(
                name: "accounts_usersexp");

            migrationBuilder.DropTable(
                name: "accounts_usertype");

            migrationBuilder.DropTable(
                name: "ad_advertisement");

            migrationBuilder.DropTable(
                name: "ad_advertiseposition");

            migrationBuilder.DropTable(
                name: "cms_channel");

            migrationBuilder.DropTable(
                name: "cms_content");

            migrationBuilder.DropTable(
                name: "cms_module");

            migrationBuilder.DropTable(
                name: "ms_configsys");

            migrationBuilder.DropTable(
                name: "ms_dic");

            migrationBuilder.DropTable(
                name: "ms_favorite");

            migrationBuilder.DropTable(
                name: "ms_guestbook");

            migrationBuilder.DropTable(
                name: "ms_hotkeywords");

            migrationBuilder.DropTable(
                name: "ms_images");

            migrationBuilder.DropTable(
                name: "ms_log");

            migrationBuilder.DropTable(
                name: "ms_regions");

            migrationBuilder.DropTable(
                name: "ms_sitemessage");

            migrationBuilder.DropTable(
                name: "ms_thumbnailsize");

            migrationBuilder.DropTable(
                name: "Userinvite");
        }
    }
}
