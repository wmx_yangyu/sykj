﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Sykj.Repository;

namespace Sykj.Repository.Migrations
{
    [DbContext(typeof(SyDbContext))]
    partial class SyDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.2-rtm-30932")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Sykj.Entity.Advertisement", b =>
                {
                    b.Property<int>("AdvertisementId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdvHtml");

                    b.Property<int>("AdvPositionId");

                    b.Property<string>("AdvertisementName")
                        .IsRequired();

                    b.Property<string>("AlternateText");

                    b.Property<int?>("AutoStop");

                    b.Property<int>("ContentType");

                    b.Property<double?>("CpmPrice");

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("CreateUserId");

                    b.Property<int?>("DayMaxIp");

                    b.Property<int?>("DayMaxPv");

                    b.Property<DateTime?>("EndDate");

                    b.Property<string>("FileUrl");

                    b.Property<int?>("Impressions");

                    b.Property<string>("NavigateUrl");

                    b.Property<string>("OperationType");

                    b.Property<int?>("Sort");

                    b.Property<DateTime?>("StartDate");

                    b.Property<int>("Status");

                    b.Property<int?>("TargetId");

                    b.HasKey("AdvertisementId");

                    b.ToTable("ad_advertisement");
                });

            modelBuilder.Entity("Sykj.Entity.Advertiseposition", b =>
                {
                    b.Property<int>("AdvPositionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdvHtml");

                    b.Property<string>("AdvPositionName")
                        .IsRequired();

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("CreateUserId");

                    b.Property<int?>("Height");

                    b.Property<bool>("IsOne");

                    b.Property<int?>("RepeatColumns")
                        .IsRequired();

                    b.Property<int>("ShowType");

                    b.Property<int?>("TimeInterval");

                    b.Property<int?>("Width");

                    b.HasKey("AdvPositionId");

                    b.ToTable("ad_advertiseposition");
                });

            modelBuilder.Entity("Sykj.Entity.Channel", b =>
                {
                    b.Property<int>("ChannelId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ChanneName")
                        .IsRequired();

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("CreateUserId");

                    b.Property<int?>("Depth");

                    b.Property<string>("Description");

                    b.Property<string>("ImageUrl");

                    b.Property<bool>("IsHasChildren");

                    b.Property<string>("Keywords");

                    b.Property<string>("MetaDescription");

                    b.Property<string>("MetaKeywords");

                    b.Property<string>("MetaTitle");

                    b.Property<int>("ModuleId");

                    b.Property<int>("ParentId");

                    b.Property<string>("Path");

                    b.Property<string>("Remark");

                    b.Property<int>("Sort");

                    b.Property<int>("Status");

                    b.HasKey("ChannelId");

                    b.ToTable("cms_channel");
                });

            modelBuilder.Entity("Sykj.Entity.Configsys", b =>
                {
                    b.Property<int>("CsId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("Description");

                    b.Property<string>("KeyName")
                        .IsRequired();

                    b.Property<int>("KeyType");

                    b.Property<string>("KeyValue")
                        .IsRequired();

                    b.HasKey("CsId");

                    b.ToTable("ms_configsys");
                });

            modelBuilder.Entity("Sykj.Entity.Content", b =>
                {
                    b.Property<int>("ContentId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BeFrom");

                    b.Property<int>("ChannelId");

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("CreateUserId");

                    b.Property<string>("Description");

                    b.Property<string>("FileName");

                    b.Property<string>("ImageUrl");

                    b.Property<bool>("IsColor");

                    b.Property<bool>("IsHot");

                    b.Property<bool>("IsRecomend");

                    b.Property<bool>("IsTop");

                    b.Property<string>("Keywords");

                    b.Property<DateTime?>("LastEditDate");

                    b.Property<int?>("LastEditUserId");

                    b.Property<string>("LinkUrl");

                    b.Property<string>("MetaDescription");

                    b.Property<string>("MetaKeywords");

                    b.Property<string>("MetaTitle");

                    b.Property<int>("PvCount");

                    b.Property<string>("Remary");

                    b.Property<int>("Sort");

                    b.Property<string>("StaticUrl");

                    b.Property<int>("Status");

                    b.Property<string>("SubTitle");

                    b.Property<string>("Summary");

                    b.Property<string>("ThumbImageUrl");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<int>("TotalComment");

                    b.Property<int>("TotalFav");

                    b.Property<int>("TotalShare");

                    b.Property<int>("TotalSupport");

                    b.Property<int>("Type");

                    b.HasKey("ContentId");

                    b.ToTable("cms_content");
                });

            modelBuilder.Entity("Sykj.Entity.Dic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreateDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("ParentId");

                    b.Property<string>("Remark")
                        .HasMaxLength(500);

                    b.Property<string>("Signs")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("Types");

                    b.Property<int?>("UserId");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.ToTable("ms_dic");
                });

            modelBuilder.Entity("Sykj.Entity.Favorite", b =>
                {
                    b.Property<int>("FavoriteId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Remark")
                        .HasMaxLength(255);

                    b.Property<string>("Tags")
                        .HasMaxLength(255);

                    b.Property<int>("TargetId");

                    b.Property<int>("Type");

                    b.Property<int>("UserId");

                    b.HasKey("FavoriteId");

                    b.ToTable("ms_favorite");
                });

            modelBuilder.Entity("Sykj.Entity.Guestbook", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AuditStatus");

                    b.Property<int>("BrowseCount");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description")
                        .HasMaxLength(65535);

                    b.Property<DateTime?>("HandlerDate");

                    b.Property<string>("HandlerNickName")
                        .HasMaxLength(255);

                    b.Property<int?>("HandlerUserId");

                    b.Property<string>("NickName")
                        .HasMaxLength(255);

                    b.Property<int>("ParentId");

                    b.Property<int>("Privacy");

                    b.Property<string>("ReplyDescription")
                        .HasMaxLength(65535);

                    b.Property<int>("Status");

                    b.Property<string>("Title")
                        .HasMaxLength(500);

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.ToTable("ms_guestbook");
                });

            modelBuilder.Entity("Sykj.Entity.HotKeyWords", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<string>("KeyWords")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("ms_hotkeywords");
                });

            modelBuilder.Entity("Sykj.Entity.Images", b =>
                {
                    b.Property<int>("ImageId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("FileExt");

                    b.Property<string>("FileName");

                    b.Property<string>("FileSize");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("TableName");

                    b.Property<int>("TargetId");

                    b.Property<string>("ThumbnailUrl");

                    b.HasKey("ImageId");

                    b.ToTable("ms_images");
                });

            modelBuilder.Entity("Sykj.Entity.Log", b =>
                {
                    b.Property<int>("LogId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("Detail");

                    b.Property<string>("IpAddress");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.Property<string>("Url");

                    b.Property<string>("UserId");

                    b.Property<string>("UserName");

                    b.HasKey("LogId");

                    b.ToTable("ms_log");
                });

            modelBuilder.Entity("Sykj.Entity.Module", b =>
                {
                    b.Property<int>("ModuleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TypeName");

                    b.HasKey("ModuleId");

                    b.ToTable("cms_module");
                });

            modelBuilder.Entity("Sykj.Entity.Permissions", b =>
                {
                    b.Property<int>("PermissionId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("Depth");

                    b.Property<string>("Description");

                    b.Property<string>("Icon");

                    b.Property<bool>("IsEnable");

                    b.Property<int>("ParentId");

                    b.Property<string>("Path");

                    b.Property<int>("Sort");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<int>("Type");

                    b.Property<string>("Url")
                        .IsRequired();

                    b.HasKey("PermissionId");

                    b.ToTable("accounts_permissions");
                });

            modelBuilder.Entity("Sykj.Entity.PointsDetail", b =>
                {
                    b.Property<int>("PointId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int>("CurrentPoints");

                    b.Property<string>("Description")
                        .HasMaxLength(255);

                    b.Property<int>("RuleId");

                    b.Property<int>("Score");

                    b.Property<int>("Type");

                    b.Property<int>("UserId");

                    b.HasKey("PointId");

                    b.ToTable("accounts_pointsdetail");
                });

            modelBuilder.Entity("Sykj.Entity.Regions", b =>
                {
                    b.Property<int>("AreaId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Depth");

                    b.Property<string>("ParentId");

                    b.Property<string>("Path");

                    b.Property<string>("RegionId");

                    b.Property<string>("RegionName");

                    b.Property<int>("Sort");

                    b.Property<string>("Spell");

                    b.HasKey("AreaId");

                    b.ToTable("ms_regions");
                });

            modelBuilder.Entity("Sykj.Entity.Rolepermissions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("PermissionId");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.ToTable("accounts_rolepermissions");
                });

            modelBuilder.Entity("Sykj.Entity.Roles", b =>
                {
                    b.Property<int>("RoleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.HasKey("RoleId");

                    b.ToTable("accounts_roles");
                });

            modelBuilder.Entity("Sykj.Entity.SiteMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<string>("Ext1")
                        .HasMaxLength(300);

                    b.Property<string>("Ext2")
                        .HasMaxLength(300);

                    b.Property<string>("MsgType")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ReadTime");

                    b.Property<bool?>("ReaderIsDel");

                    b.Property<int?>("ReceiverID");

                    b.Property<bool?>("ReceiverIsRead");

                    b.Property<DateTime>("SendTime");

                    b.Property<int?>("SenderID");

                    b.Property<bool?>("SenderIsDel");

                    b.Property<string>("Title")
                        .HasMaxLength(300);

                    b.HasKey("Id");

                    b.ToTable("ms_sitemessage");
                });

            modelBuilder.Entity("Sykj.Entity.ThumbnailSize", b =>
                {
                    b.Property<int>("ThumId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("CreateUserID");

                    b.Property<bool>("IsWatermark");

                    b.Property<string>("Remark");

                    b.Property<int>("ThumHeight");

                    b.Property<string>("ThumMode");

                    b.Property<string>("ThumName");

                    b.Property<int>("ThumWidth");

                    b.Property<int>("Type");

                    b.HasKey("ThumId");

                    b.ToTable("ms_thumbnailsize");
                });

            modelBuilder.Entity("Sykj.Entity.Userinvite", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int>("Depth");

                    b.Property<string>("InviteNick")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("InviteType");

                    b.Property<int>("InviteUserId");

                    b.Property<bool>("IsNew");

                    b.Property<bool>("IsRebate");

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("RebateDesc")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Remark")
                        .HasMaxLength(500);

                    b.Property<int>("Status");

                    b.Property<int>("UserId");

                    b.Property<string>("UserNick")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Userinvite");
                });

            modelBuilder.Entity("Sykj.Entity.UserRoles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("RoleID");

                    b.Property<int>("UserID");

                    b.HasKey("Id");

                    b.ToTable("accounts_userroles");
                });

            modelBuilder.Entity("Sykj.Entity.Users", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Activity");

                    b.Property<string>("AppOpenId")
                        .HasMaxLength(100);

                    b.Property<DateTime>("CreateDate");

                    b.Property<int?>("Creator");

                    b.Property<string>("DepartmentId")
                        .HasMaxLength(100);

                    b.Property<string>("Email")
                        .HasMaxLength(100);

                    b.Property<string>("Gravatar")
                        .HasMaxLength(255);

                    b.Property<string>("NickName")
                        .HasMaxLength(100);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Phone")
                        .HasMaxLength(100);

                    b.Property<string>("Sex")
                        .HasMaxLength(50);

                    b.Property<string>("TrueName")
                        .HasMaxLength(100);

                    b.Property<string>("UnionId")
                        .HasMaxLength(100);

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("UserType")
                        .HasMaxLength(50);

                    b.Property<string>("WechatOpenId")
                        .HasMaxLength(100);

                    b.HasKey("UserId");

                    b.ToTable("accounts_users");
                });

            modelBuilder.Entity("Sykj.Entity.Usersexp", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(200);

                    b.Property<decimal?>("Balance");

                    b.Property<DateTime>("Birthday");

                    b.Property<string>("CardId")
                        .HasMaxLength(100);

                    b.Property<DateTime?>("ExpireTime");

                    b.Property<int>("Grade");

                    b.Property<string>("HomePage")
                        .HasMaxLength(50);

                    b.Property<bool?>("IsUserDPI");

                    b.Property<DateTime?>("LastLoginDate");

                    b.Property<string>("NativePlace")
                        .HasMaxLength(50);

                    b.Property<int?>("Points");

                    b.Property<string>("QQ")
                        .HasMaxLength(50);

                    b.Property<string>("RegionId")
                        .HasMaxLength(100);

                    b.Property<string>("Remark");

                    b.Property<string>("Singature")
                        .HasMaxLength(200);

                    b.Property<int?>("SourceType");

                    b.Property<string>("TelPhone")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdateDate");

                    b.Property<string>("UserCardCode")
                        .HasMaxLength(50);

                    b.Property<int?>("UserCardType");

                    b.HasKey("UserId");

                    b.ToTable("accounts_usersexp");
                });

            modelBuilder.Entity("Sykj.Entity.Usertype", b =>
                {
                    b.Property<string>("UserType")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.HasKey("UserType");

                    b.ToTable("accounts_usertype");
                });
#pragma warning restore 612, 618
        }
    }
}
