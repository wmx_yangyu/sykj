﻿using Sykj.Infrastructure;
using Sykj.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;

namespace Sykj.Services
{
    /// <summary>
    /// 会员 bjg
    /// </summary>
    public class UserMember : Sykj.Repository.RepositoryBase<Sykj.Entity.Usersexp>, Sykj.IServices.IUserMember
    {
        public UserMember(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {
            
        }

        #region  注册用户  bjg
        /// <summary>
        /// 注册用户  bjg
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="passWord">密码</param>
        /// <param name="nikeName">昵称</param>
        /// <param name="wechatOpenId">微信openId</param>
        /// <param name="unionId">腾讯平台用户唯一标识</param>
        /// <returns></returns>
        public int Register(string userName, string passWord, string nikeName = "", string wechatOpenId = "", string unionId = "",string gavatarUrl = "",int inviteUserId=0)
        {
            var tran = _dbContext.Database.BeginTransaction();
            try
            {
                //会员表
                var userModel = new Entity.Users()
                {
                    NickName = nikeName,
                    UserName = userName,
                    TrueName = userName,
                    Phone = userName,
                    WechatOpenId = wechatOpenId,
                    UnionId = unionId,
                    Password = DESEncrypt.Encrypt(passWord),
                    UserType = "UU",
                    Gravatar=gavatarUrl,
                    CreateDate = DateTime.Now,
                    Activity = true
                };
                _dbContext.Users.Add(userModel);
                _dbContext.SaveChanges();

                //会员扩展表
                var userExp = new Entity.Usersexp()
                {
                    UserId = userModel.UserId,
                    HomePage = "",
                    Birthday = DateTime.Now,
                    NativePlace = "",
                    TelPhone = userName,
                    LastLoginDate = DateTime.Now
                };
                _dbContext.Usersexp.Add(userExp);

                //推广信息表
                if (inviteUserId>0)
                {
                    var inviteModel = new Entity.Userinvite();
                    inviteModel.UserId = userModel.UserId;
                    inviteModel.UserNick = userName;
                    inviteModel.InviteUserId = inviteUserId;
                    inviteModel.InviteNick = "";
                    inviteModel.Depth = 0;
                    inviteModel.Path = "";
                    inviteModel.CreatedDate = DateTime.Now;
                    inviteModel.InviteType =2;//邀请注册类型
                    inviteModel.IsNew = true;
                    inviteModel.IsRebate = false;
                    inviteModel.RebateDesc = "";
                    inviteModel.Remark = "";
                    inviteModel.Status =1;
                    _dbContext.Userinvite.Add(inviteModel);
                    _dbContext.SaveChanges();
                }
                tran.Commit();
                return userModel.UserId;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return 0;
            }
        }
        #endregion

        #region 分页查询
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public IQueryable<Sykj.ViewModel.UserMember> GetMemberList(int pageIndex,
            int pageSize, string predicate, string ordering, params object[] args)
        {
            var result = from a in _dbContext.Usersexp
                         join b in _dbContext.Users on a.UserId equals b.UserId 
                         select new Sykj.ViewModel.UserMember
                         {
                             UserId = b.UserId,
                             UserName = b.UserName,
                             NickName = b.NickName,
                             TrueName = b.TrueName,
                             Password = b.Password,
                             UnionId = b.UnionId,
                             Sex = b.Sex,
                             Phone = b.Phone,
                             Email = b.Email,
                             Activity = b.Activity,
                             Gravatar = b.Gravatar,
                             UserType = b.UserType,
                             Singature = a.Singature,
                             TelPhone = a.TelPhone,
                             QQ = a.QQ,
                             HomePage = a.HomePage,
                             Birthday = a.Birthday,
                             NativePlace = a.NativePlace,
                             Address = a.Address,
                             Grade = a.Grade,
                             Balance = a.Balance,
                             Points = a.Points,
                             IsUserDPI = a.IsUserDPI,
                             UserCardCode = a.UserCardCode,
                             UserCardType = a.UserCardType,
                             SourceType = a.SourceType,
                             CardId = a.CardId,
                             UpdateDate = a.UpdateDate,
                             CreateDate = b.CreateDate,
                             Remark = a.Remark,
                             LastLoginDate = a.LastLoginDate
                         };
            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);

            if (!string.IsNullOrWhiteSpace(ordering))
                result = result.OrderBy(ordering);

            return result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
        #endregion

        #region 数据条数
        /// <summary>
        /// 数据条数
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public int GetMemberRecordCount(string predicate, params object[] args)
        {
            var result = from a in _dbContext.Usersexp
                         join b in _dbContext.Users on a.UserId equals b.UserId 
                         select new Sykj.ViewModel.UserMember
                         {
                             UserId = b.UserId,
                             UserName = b.UserName,
                             NickName = b.NickName,
                             TrueName = b.TrueName,
                             Password = b.Password,
                             UnionId = b.UnionId,
                             Sex = b.Sex,
                             Phone = b.Phone,
                             Email = b.Email,
                             Activity = b.Activity,
                             Gravatar = b.Gravatar,
                             UserType = b.UserType,
                             Singature = a.Singature,
                             TelPhone = a.TelPhone,
                             QQ = a.QQ,
                             HomePage = a.HomePage,
                             Birthday = a.Birthday,
                             NativePlace = a.NativePlace,
                             Address = a.Address,
                             Grade = a.Grade,
                             Balance = a.Balance,
                             Points = a.Points,
                             IsUserDPI = a.IsUserDPI,
                             UserCardCode = a.UserCardCode,
                             UserCardType = a.UserCardType,
                             SourceType = a.SourceType,
                             CardId = a.CardId,
                             UpdateDate = a.UpdateDate,
                             CreateDate = b.CreateDate,
                             Remark = a.Remark,
                             LastLoginDate = a.LastLoginDate
                         };
            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);

            return result.Count();
        }
        #endregion

        #region 获得用户信息
        /// <summary>
        /// 用户信息
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public Sykj.ViewModel.UserMember GetMemberModel(Expression<Func<Sykj.ViewModel.UserMember, bool>> predicate)
        {
            var result = from a in _dbContext.Usersexp
                         join b in _dbContext.Users on a.UserId equals b.UserId
                         select new Sykj.ViewModel.UserMember
                         {
                             UserId = a.UserId,
                             UserName = b.UserName,
                             NickName = b.NickName,
                             TrueName = b.TrueName,
                             Password = b.Password,
                             UnionId = b.UnionId,
                             Sex = b.Sex,
                             Phone = b.Phone,
                             Email = b.Email,
                             Activity = b.Activity,
                             Gravatar = b.Gravatar,
                             UserType = b.UserType,
                             Singature = a.Singature,
                             TelPhone = a.TelPhone,
                             QQ = a.QQ,
                             HomePage = a.HomePage,
                             Birthday = a.Birthday,
                             NativePlace = a.NativePlace,
                             Address = a.Address,
                             Grade = a.Grade,
                             Balance = a.Balance,
                             Points = a.Points,
                             IsUserDPI = a.IsUserDPI,
                             UserCardCode = a.UserCardCode,
                             UserCardType = a.UserCardType,
                             SourceType = a.SourceType,
                             CardId = a.CardId,
                             UpdateDate = a.UpdateDate,
                             Remark = a.Remark,
                             LastLoginDate = a.LastLoginDate
                         };
            return result.FirstOrDefault(predicate);
        }
        #endregion

    }
}
