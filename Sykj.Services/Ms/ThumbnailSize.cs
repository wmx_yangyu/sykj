﻿using Sykj.Infrastructure;
using Sykj.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sykj.Services
{
    /// <summary>
    /// 缩略图
    /// </summary>
    public class ThumbnailSize : Sykj.Repository.RepositoryBase<Sykj.Entity.ThumbnailSize>, Sykj.IServices.IThumbnailSize
    {
        ICacheService _cacheService;

        public ThumbnailSize(Sykj.Repository.SyDbContext dbcontext, ICacheService cacheService) : base(dbcontext)
        {
            _cacheService = cacheService;
        }

        /// <summary>
        /// 从缓存中获取数据所有集合
        /// </summary>
        /// <returns>返回满足查询条件的list</returns>
        public List<Sykj.Entity.ThumbnailSize> GetListByCache()
        {
            List<Sykj.Entity.ThumbnailSize> list = new List<Entity.ThumbnailSize>();

            if (_cacheService.Exists(CacheKey.THUMBNAILSIZEALL))
            {
                list = _cacheService.GetCache<List<Sykj.Entity.ThumbnailSize>>(CacheKey.THUMBNAILSIZEALL);
            }
            else
            {
                list = GetList().ToList();
                _cacheService.SetCache(CacheKey.THUMBNAILSIZEALL, list);
            }

            return list;
        }

        /// <summary>
        /// 根据模块选择
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Sykj.Entity.ThumbnailSize> GetThumSizeList(EnumHelper.UpLoadType type)
        {
            var list = GetListByCache().Where(c => c.Type == (int)type);
            return list.ToList();
        }
    }
}
